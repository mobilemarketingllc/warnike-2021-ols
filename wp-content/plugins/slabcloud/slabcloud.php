<?php

/**
 * @package Slabcloud Inventory Plugin
 * @version 1.8.2
 */
/*
		Plugin Name: Slabcloud Inventory Plugin
		Plugin URI: https://slabcloud.com
		Description: Material Inventory Listing
		Author: Slabcloud
		Version: 1.8.2
		Author URI: https://slabcloud.com
	*/


define('SLABCLOUD_REQUIRED_JQUERY_VERSION', '1.8.0');
define('SLABCLOUD_PLUGIN_URL',  '/wp-content/plugins/' . dirname(plugin_basename(__FILE__)) . '/');

add_shortcode('slabcloud', 'slabcloud_gallery_shortcode');

function slabcloud_inventory_modals()
{
	echo '
			<div  style="display:none;" class="sc-modal" id="slab-page"></div>			
			<div  style="display:none;" class="sc-modal2">			
			<div  class="sc-sharer">
			<div class="sc-modal-close sharer-close"><img class="sc-icon" src="https://slabcloud.com/assets/images/inventory/icons/close.png"></div>	
			<div class="sharer-title"> <h2>Share this slab</h2></div>			
			<div id="share-buttons">			
			</div>	
			</div>
			</div>				
			<div  style="display:none;" class="sc-modal3">
			<div class="sc-3d-wrapper">
			<div class="sc-modal-close 3d-close"><img class="sc-icon" src="https://slabcloud.com/assets/images/inventory/icons/close.png"></div>			
			<div class="edgeFrame"></div>			
			</div>
			</div>	
			<div  style="display:none;" class="sc-modal4">
			<div class="inquiry-form-wrapper">
			<div class="inv-loader sc-loader-round inquiry"></div>			
			<div class="sc-modal-close inquiry-form-close"><img class="sc-icon" src="https://slabcloud.com/common/images/icons/close.png"></div>				
			<div id="inquiry-form"></div>				
			</div>
			</div>	
			
				<div  style="display:none;" class="sc-modal5">
				<div class="measure-wrapper">					
					<div class="sc-modal-close measure-close"><img class="sc-icon" src="https://slabcloud.com/common/images/icons/close.png"></div>						
					<div class="slab-measure"><iframe id="slab-measure" src=""></iframe> </div>						
				</div>
			</div>		
			
					<div  style="display:none;" class="sc-modal6">
				<div class="measure-wrapper">			
					<div class="sc-modal-close bookmatch-close"><img class="sc-icon" src="https://slabcloud.com/common/images/icons/close.png"></div>				
					<div class="slab-measure"><iframe id="slab-bookmatch" src=""></iframe> </div>	
								</div>
			</div>	

			<div class="sc-modal7">
			<div class="viz-wrapper">
				<div class="sc-modal-close visualizer-close"><img class="sc-icon" src="https://slabcloud.com/common/images/icons/close.png"></div>				
				<div id="viz-wrapper"></div>				
			</div>
		</div>	
				
			

			';
}

function slabcloud_gallery_shortcode($atts)
{
	$selectedMaterial = false;
	$selectedType = false;

	$developMode = false;
	if (!empty($atts['development'])) {
		$developMode = true;
	}






	add_action('wp_footer', 'slabcloud_inventory_modals');

	if (!$atts) {
		$atts = array();
	}
	$selectedMaterial = '';
	if (!empty($atts['material'])) {

		$selectedMaterial = ' data-material="' . $atts['material'] . '" ';
	}
	if (!empty($atts['type'])) {

		$selectedType = ' data-type="' . $atts['type'] . '" ';
	}

	$hideTypeFilter = '';
	if (!empty($atts['hidetypefilter'])) {
		$hideTypeFilter = ' style="display:none;" ';
	}
	$notags = '';
	if (!empty($atts['notags'])) {
		$notags = ' data-notags="true" ';
	}


	$out = '
		<div style="display:none;"  class="filter-sticky"' . $selectedMaterial . $selectedType . $notags . '>		
		<div class="sc-container">		
		<div class="sort-group">		
		<div class="collection-sort ">		
		<div data-color="brown" class="brown  color-select" title="Brown/Yellow Colors"></div>
							<div data-color="gray" class="grey color-select" title="Gray Colors"></div>
							<div data-color="white" class="white color-select" title="White Colors"></div>
							<div data-color="black" class="black color-select" title="Black Colors"></div>
							<div style="display:none;" data-color="green" class="green color-select" title="Green Colors"></div>
							<div  style="display:none;" data-color="blue" class="blue color-select" title="Blue Colors"></div>
							
		</div>
		
		<div title="Filter" class="sc-icon filter-btn sc-flt sc-cell"><img src="https://slabcloud.com/common/images/icons/filter-blue.png" /></div>
		<div title="Favorites" class="sc-icon favorites-btn sc-cell sc-flt"><img class="svg" src="https://slabcloud.com/common/images/icons/heart-gray.png" /></div>
		<a title="Download PDF" style="display:none;"  mode-fav href="https://slabcloud.com/api/pdf/wordpress?type=favorites" class="sc-icon fav-e pdf-btn sc-cell fav-bar" target="_blank" download><img  src="https://slabcloud.com/common/images/icons/pdf2.svg" /></a>
		<div class="sc-icon  fav-close-btn sc-cell " style="display:none;" ><img class="svg" src="https://slabcloud.com/common/images/icons/save01.png" /></div>
		<div title="Clear Favorites List" class="sc-icon fav-e fav-clear-btn sc-cell fav-bar" style="display:none;" ><img class="svg" src="https://slabcloud.com/common/images/icons/trash.png" /></div>
		
		
		
		<div style="display:none;" title="Switch to Thumb View" class="sc-icon thumbs-btn mode-selection sc-cell"><img src="https://slabcloud.com/common/images/icons/thumbs-view.svg" /></div>
		<div style="display:none;" title="Switch to List View" class="sc-icon list-btn  mode-selection sc-cell"><img src="https://slabcloud.com/common/images/icons/list-view.png" /></div>
		<div title="Close Favorites" class="sc-icon  fav-close-btn sc-cell fav-bar" style="display:none;" ><img class="svg" src="https://slabcloud.com/common/images/icons/close.png" /></div>
		</div>	
		
		</div>
		
		
		<div class="sc-container">
		<div class="advanced-filter">
		
		<select class="sc-resetable"  name="material-select" id="material-select">
		<option value="">All Materials</option>	
		</select>
		
			<select style="display:none;" class="sc-resetable" name="thickness-select" id="thickness-select">
		<option  selected="" value="">Any Thickness</option>
		<option value="2 cm">2 cm</option>		
		<option value="3 cm">3 cm</option>	
		</select>
		
		<select ' . $hideTypeFilter . ' class="sc-resetable" name="type-select" id="type-select">
		<option  selected="" value="">Any Type</option>
		<option value="Slab">Full Slabs</option>		
		<option value="Remnant">Remnants</option>
		</select>
		
		
		
		<input id="sc-search" type="text" name="q" class="slab-search" placeholder="Search (Name or ID)" /><span class="search-reset">x</span>
		
		<div class="sc-row-d">		
		<div class="range-slider sc-cell-d">
		<label for="min-height" class="range-label">Min Length <span id="length-value"></span></label>
		<div id="minLength"></div>
		
		</div>	
		<div class="range-slider sc-cell-d">
		<label for="min-width" class="range-label">Min Width <span id="width-value" ></span></label>
		<div id="minWidth"></div>
		
		</div>						
		
		<div class="sc-cell-d f-reset"><a class="filter-reset " href="#reset" >Reset</a></div>
		</div>						
		
		<div class="filter-pull-up"><img src="https://slabcloud.com/common/images/icons/arrow-up.png" /></div>						
		</div>	
		</div>
		</div>
		
		

		
		<div class="sc-container">
		<div id="inventory-loader"><div class="inv-loader sc-loader-round"></div></div>
		<div class="inventory-spacer"></div>
		<section class="products noscroll" id="inventory">
		</section>		
		<div class="powered">Powered by <a style="text-decoration:none; color:#0973b3; " href="https://slabcloud.com" target="_blank" title="Websites for stone fabricators. Slabsmith Inventory on your website"><span >SlabCloud</span></a></div>
		</div>
		';


	if (!$atts) {
		$atts = array();
	}

	if (array_key_exists('slabcloud', $atts) && $atts['slabcloud'] == 'false') {
		return gallery_shortcode($atts);
	}
	wp_enqueue_script('jquery');
	slabcloud_scripts($developMode);

	return $out;
}





function slabcloud_scripts($developMode)
{




	addStyle('Robotto.css', 'https://fonts.googleapis.com/css?family=Roboto:300,400');
	addStyle('style.wp.1.1.css', 'https://slabcloud.com/inventory/warnike/css/style.wp.2.1.css', $developMode);
	addStyle('magnify.min.css', 'https://slabcloud.com/assets/css/inventory/magnify.min.css');
	addStyle('nouislider.min.css', 'https://slabcloud.com/common/css/nouislider.min.css');



	//	addScript('jquery.min.js','https://slabcloud.com/common/js/jquery-3.1.1.min.js');			

	//addScript('jquery.lazyload.min.js','https://slabcloud.com/common/js/jquery.lazyload.min.js');	
	addScript('lazyload.2.min.js', 'https://slabcloud.com/common/js/lazyload.2.min.js');

	addScript('sc-share.js', 'https://slabcloud.com/assets/js/inventory/sc-share.js');
	addScript('magnify2.js', 'https://slabcloud.com/common/js/magnify2.js');
	addScript('nouislider.min.js', 'https://slabcloud.com/common/js/nouislider.min.js');
	addScript('slabcloud.wp.js', 'https://slabcloud.com/inventory/warnike/js/slabcloud.wp.2.1.js', $developMode);
}


function addScript($file, $source, $random = '')
{
	if (!empty($random)) {
		$random = '?version=' . rand(1, 99999);
	}
	wp_register_script($file, $source . $random, array('jquery'));
	wp_enqueue_script($file);
}



function addStyle($file, $source, $random = '')

{
	if (!empty($random)) {
		$random = '?version=' . rand(1, 99999);
	}
	wp_register_style($file, $source . $random);
	wp_enqueue_style($file);
}
