import React from "react";

export default function ShapeFacet({ handleFilterClick, productShape }) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productShape = sortObject(productShape);

  return (
    <div class="facet-wrap facet-display">
      <strong>Shape</strong>
      <div className="facetwp-facet">
        {Object.keys(productShape).map((shape, i) => {
          if (shape && productShape[shape] > 0) {
            return (
              <div>
                <span
                  id={`shape-filter-${i}`}
                  key={i}
                  data-value={`${shape.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("shape_facet", e.target.dataset.value)
                  }>
                  {" "}
                  {shape} {` (${productShape[shape]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
