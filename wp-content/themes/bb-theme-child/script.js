 jQuery( document ).ready(function() {
    jQuery('.searchIcon .fl-icon').click(function(){
        jQuery('.searchModule').slideToggle();
    });    

    jQuery(document).mouseup(function(e) {
        var container = jQuery(".searchModule, .searchIcon");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0 && jQuery('.searchModule').css('display') !=='none') 
        {
            jQuery('.searchModule').slideToggle();
        }
    });
});

var $ = jQuery;

function positionAppend(position){
    var ele = $(".position_class input[type=text]")
    ele.attr('readonly', false);
    ele.attr('value', position);
    ele.attr('readonly', true);
}  